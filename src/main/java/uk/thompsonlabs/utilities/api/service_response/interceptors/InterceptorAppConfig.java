package uk.thompsonlabs.utilities.api.service_response.interceptors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import uk.thompsonlabs.utilities.api.service_response.interceptors.auth.APIPassportAuthInterceptor;
import uk.thompsonlabs.utilities.api.service_response.interceptors.auth.AuthInterceptor;
import uk.thompsonlabs.utilities.api.service_response.interceptors.timing.TimingInterceptor;

@Component
public class InterceptorAppConfig implements WebMvcConfigurer {

    @Autowired
    AuthInterceptor authInterceptor;

    @Autowired
    APIPassportAuthInterceptor apiPassportAuthInterceptor;

    @Autowired
    TimingInterceptor timingInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        registry.addInterceptor(authInterceptor);

        registry.addInterceptor(timingInterceptor);

        registry.addInterceptor(apiPassportAuthInterceptor);
    }
}
