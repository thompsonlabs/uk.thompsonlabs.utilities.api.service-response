package uk.thompsonlabs.utilities.api.service_response.interceptors.auth;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.time.temporal.ChronoUnit.SECONDS;


@Component
public class APIPassportAuthInterceptor implements HandlerInterceptor {

    @Autowired
    private Environment springEnvironment;

    private final HttpClient httpClient = HttpClient.newHttpClient();

    private final Gson gson = new Gson();

    /**
     * The main Fibre middleware handler function,
     * responsible for parsing the APP_ID header from each
     * inbound request and proceeding to call into the
     * api-passport-client (sidecar), which will be running on
     * localhost, to validate it. The api-passport-client will then
     * communicate with a remote api-passport-server, as necessary,
     * to come to this determination,
     * */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        //before we proceed first check whether api passport security has been enabled if not we immediately transfer
        //control to our parent class handler which essentially results in the call being passed-through unchanged.
        final var apiPassportSecurityEnabled = Boolean.parseBoolean(springEnvironment.getProperty("enable.api.passport.security"));
        Logger.getLogger(APIPassportAuthInterceptor.class.getName()).log(Level.INFO,"API Passport Security Enabled = "+apiPassportSecurityEnabled);
        if(!apiPassportSecurityEnabled){
            return HandlerInterceptor.super.preHandle(request,response,handler);
        }


        HandlerMethod hm;

        try {

            hm = (HandlerMethod) handler;
        }
        catch (ClassCastException e) {

            return HandlerInterceptor.super.preHandle(request, response, handler);
        }

        Method method = hm.getMethod();

        var methodName = method.getName();

        //exclude internal Spring Framework methods
        if(methodName.equals("uiConfiguration")
                || methodName.equals("securityConfiguration")
                || methodName.equals("swaggerResources")
                || methodName.equals("getDocumentation") )
            return  HandlerInterceptor.super.preHandle(request, response, handler);

        //attempt to grab reference to the API Passport Auth Annotation.
        final APAuth apAuthAnnotation = method.getAnnotation(APAuth.class);

        if(apAuthAnnotation == null) {
            //if the APIPassport Auth annotation could not be found we just pass the request through unchanged,
            return HandlerInterceptor.super.preHandle(request, response, handler);
        }


        //otherwise where the API Passport Auth annotation exists we continue processing....

        //log status
        Logger.getLogger(APIPassportAuthInterceptor.class.getName()).log(Level.INFO,"API Passport APAuth Annotation found! Authenticating request, Standby...");

        //parse APP_ID header from the request
        final var appID = request.getHeader("APP_ID");

        //if an app id has not been provided as indicated by a null value being returned,
        //we abort the operation and trow an AuthenticationException which will ultimately result
        // in  a 401 error being returned to the caller.
        if(appID == null){
           throw new AuthContext.AuthenticationException("Unauthorised. An APP_ID was not specified.");
        }

        //conversely, where we DO have an APP_ID value, call into our helper function
        //to attempt to validate it with the api-passport platform. The function
        //will return bool, true or false respectively.
        var idValid = false;
        try {
            idValid = this.validateAppIdWithApiPassportClient(appID);
        }
        catch(Exception ex){
            throw new AuthContext.AuthenticationException("Unauthorised. The call to the downstream api-passport platform failed; the request could not be authenticated.",ex);
        }

        //if the APP_ID was not valid we throw an Authorization exception which will result in a 403
        // error being returned to the caller; conversely, if the id is valid we allow the request to fall
        //through to the next handler
        if (!idValid) {
            throw new AuthContext.AuthorizationException("Forbidden. An incorrect APP_ID value was specified.");
        }

        //on success, log status and  pass control to the next handler in the chain.
        Logger.getLogger(APIPassportAuthInterceptor.class.getName()).log(Level.INFO,"Request was SUCCESSFULLY Authenticated via the API Passport Platform.");
        return HandlerInterceptor.super.preHandle(request, response, handler);

    }

    //validateAppIdWithApiPassportClient - Issues a call into api-passport-client (sidecar)
//                                         to determine if the provided APP_ID relates to
//                                         a valid passport. Will return TRUE where such
//                                         a passport exists and does not have its deleted date set.
    private boolean validateAppIdWithApiPassportClient(String anAppID) throws Exception{


        //Note: The API Passport Clients validate endpoint will looks be as follows:
        //localhost:[port]/api/v1/passports/validate/:app_id

        //grab port the API Passport client is listening on from the environment config.
        final var apiPassportClientPort = springEnvironment.getProperty("api.passport.client.app.port");

        //begin building path to the validate endpoint
        var apiClientSideCarHost = "http://localhost";

        //if thew provided port is not null/empty and is not port 80 append the value to our host
        if(!(apiPassportClientPort == null || apiPassportClientPort.isBlank() || apiPassportClientPort.equals("80"))){
            apiClientSideCarHost+=":".concat(apiPassportClientPort);
        }

        //build path to the api-passport-client validate endpoint
        final var validateEndpointPath = apiClientSideCarHost
                .concat("/api/v1/passports/validate/")
                .concat(anAppID);

        //call into our function to actually dispatch the validation request to the api-passport-client sidecar
        final var validateRequestResp = this.dispatchHttpGetRequest(validateEndpointPath);

        //where the request succeeded we will have a true or false value however
        //it will be wrapped in a JSON object, so we will need to parse the value.
        // i.e { "valid": "true"}
        //Note: we don't specify generic types on our map as we usually would as we want the
        //types contained in the map to be automatically inferred.
        final Map valMap = gson.fromJson(validateRequestResp, Map.class);
        final var validValue = valMap.get("valid");

        if(validValue == null)
            return false;

        return Boolean.parseBoolean((String)validValue);
    }

    private  String dispatchHttpGetRequest(String aURL) throws URISyntaxException, IOException, InterruptedException {

        //build request
        final var requestTimeout = 30L;
        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI(aURL))
                .timeout(Duration.of(requestTimeout, SECONDS))
                .GET()
                .build();

        //send request and return the response
        final var response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        return response.body();
    }


}
