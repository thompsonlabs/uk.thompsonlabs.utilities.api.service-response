package uk.thompsonlabs.utilities.api.service_response.context;


import uk.thompsonlabs.utilities.api.service_response.interceptors.auth.AuthContext;

import java.util.HashMap;
import java.util.Map;

/**
 * A thin wrapper around ThreadLocal that provides access to data associated with the
 *  current request including headers, AuthContext etc to other layes of the application,
 *  the Service layer for example.
 * */
public final class APIRequestContext {

    private final ThreadLocal<ContextData> threadLocalContext;

    private static APIRequestContext apiRequestContext;

    APIRequestContext() {

        threadLocalContext = new ThreadLocal<>();
    }

    /** Factory method, returns reference to a singleton instance of an APIRequestContext */
    public static APIRequestContext getInstance(){

        if(apiRequestContext == null)
            apiRequestContext = new APIRequestContext();

        return apiRequestContext;
    }

    /** Clears all Data associated with this APIRequestContext for the current thread.*/
    public void clearAllData(){

        threadLocalContext.remove();
    }

    public void setAuthContext(AuthContext authContext){

        this.getContextData().setAuthContext(authContext);
    }

    public AuthContext getAuthContext(){

        return this.getContextData().getAuthContext();
    }

    public void setHeaders(Map<String,String> headers){

        this.getContextData().addHeaderNameValuePairs(headers);
    }

    public Map<String,String> getHeaders(){

        return this.getContextData().getHeadersNameValuePairs();
    }

    /** Associates data with this APIRequestContext for the current thread. */
    public void putData(String key,Object value){

        this.getContextData().addToDataMap(key, value);
    }

    public Object getData(String key){

        return this.getContextData().getFromDataMap(key);
    }



    /** Logs the request start time.*/
    public void logStartTime(){

        this.getContextData().setRequestStartTime(System.currentTimeMillis());
    }

    /** Logs the request end time, then proceeds to compute and return the
     *  full request duration. */
    public long logEndTime(){

        long endTime = System.currentTimeMillis();

        this.getContextData().setRequestEndTime(endTime);

        long startTime;

        if((startTime = this.getContextData().getRequestStartTime()) <= 0)
            throw new IllegalStateException("Unable to compute request duration as a Start Time was not logged.");

        return (endTime - startTime);
    }


                                        //Helper Methods


    /** Gets ContextData for the current thread. ContextData is created where it does not already exist. */
    private ContextData getContextData(){

        ContextData ctxData;

        if((ctxData = threadLocalContext.get()) == null){

            ctxData = ContextData.newInst();

            threadLocalContext.set(ctxData);
        }

        return ctxData;
    }



                                                //Helper Classes

    static class ContextData{

        private final Map<String,String>  headersNameValuePairs;

        private AuthContext authContext;

        private long requestStartTime;

        private long requestEndTime;

        private final Map<String,Object> dataMap;

        ContextData(final Map<String, String> headersNameValuePairs, final AuthContext authContext) {

            this.headersNameValuePairs = headersNameValuePairs;
            this.authContext = authContext;
            this.dataMap = new HashMap<>();

        }

        ContextData(final AuthContext authContext) {

            this(new HashMap<String,String>(),authContext);
        }

        ContextData(){

            this(null);
        }



        static ContextData newInst(){

            return new ContextData();
        }



        Map<String, String> getHeadersNameValuePairs() {
            return headersNameValuePairs;
        }

        AuthContext getAuthContext() {
            return authContext;
        }

        void setAuthContext(AuthContext authContext) {

            this.authContext = authContext;
        }

        long getRequestStartTime() {
            return requestStartTime;
        }

        void setRequestStartTime(long requestStartTime) {
            this.requestStartTime = requestStartTime;
        }

        long getRequestEndTime() {
            return requestEndTime;
        }

        void setRequestEndTime(long requestEndTime) {
            this.requestEndTime = requestEndTime;
        }

        void addHeaderNameValuePairs(Map<String,String> headerNameValPairsMap){

                this.headersNameValuePairs.putAll(headerNameValPairsMap);
        }

        void addToDataMap(String key,Object value){

            this.dataMap.put(key, value);
        }

        Object getFromDataMap(String key){

            return this.dataMap.get(key);
        }
    }
}
