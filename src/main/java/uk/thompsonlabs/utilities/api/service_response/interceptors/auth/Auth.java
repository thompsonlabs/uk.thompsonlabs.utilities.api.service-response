package uk.thompsonlabs.utilities.api.service_response.interceptors.auth;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Auth {

    /** Specifies the minimum user level privileges required to issue calls against
     *  the endpoint this Auth annotation is associated with. Where the value
     *  in the user level header, falls below this level, the operation will be
     *  aborted and a 403 Forbidden error should be returned.*/
    int requiredUserLevel();

    /**A boolean value that indicates whether or not Auth Request Signature
     * header values, received from the API Gateway, should be validated,
     * they are by default. When set to true a local signature value for
     * each request will be computed and compared to the value received
     * from the gateway. The request, in question will ONLY be validated where
     * both signature match Where this is not the case, the requst will be
     * expressly aborted and a 403 Forbidden error will be subsequently returned
     * to the caller.
     * */
    boolean verifySignature() default true;

    /**The name of the caller (user) id header */
    String callerIdHeaderName() default "cId";

    /**The name of user level header */
    String callerUserLevelHeaderName() default "cUl";

    /**The name of the user authorisation (session id) key header. */
    String callerSessionIdHeaderName() default "X-AUTH-KEY";

    /**The name of the gateway's auth request timestamp header. The value
     * of this header will be unique to each received request to the endpoint
     * this auth annotation is associated with.*/
    String gatewayAuthRequestTimestamp() default "gw-art";

    /**The name of the gateway's auth request signature header. The value
     * of this header will be unique to each received request to the endpoint
     * this auth annotation is associated with.  */
    String gatewayAuthRequestSignature() default "gw-ars";

    /**
     * A boolean value that indicates whether internal calls (i.e. direct service to service
     * calls for services that sit behind the SAME gateway.) is enabled for an endpoint. When enabled the gatewayAuthRequestSignature
     * verification process is omitted.
     * NOTE: The internal call will ONLY succeed where the provided "internalCallId" value
     *       is EQUAL to the GatewayPrivateSignatureKey value associated with THIS service
     *       as obtained by calling: springEnvironment.getProperty("gateway.auth.request.signature.key");
     *       which essentially verifies that the two services are sat behind the SAME API Gateway.
     * */
    boolean internalCallsEnabled() default false;

    /**
     *  The NAME of the header that will hold the callers GatewayPrivateSignatureKey value.
     *  Where internal calls are enabled to an endpoint, as denoted by the
     * above "internalCallsEnabled()" annotation value being set to TRUE then
     * this value should be the GatewayPrivateSignatureKey of the
     * Gateway the CALLING service is sat behind. A local call will ONLY
     * be permitted where this value is EQUAL to the Private Signature Key
     * of the API Gateway that THIS service is sat behind.
     * That is, both services must be sat behind the SAME API Gateway for the internal call to succeed. */
    String internalCallIdHeaderName() default "X-INTERNAL-CALL-ID";


}
