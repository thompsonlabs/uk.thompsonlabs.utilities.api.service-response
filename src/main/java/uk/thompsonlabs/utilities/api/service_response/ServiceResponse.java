package uk.thompsonlabs.utilities.api.service_response;

import java.util.Collection;

/**
 *  Used to return responses from the service in a consistent
 *  format. Also allows for the inclusion of service-specific
 *  meta-data (i.e API version, query duration,etc) in the response.
 * */
public final class ServiceResponse <I> {

    private String serviceApiVersion;
    private String environment;
    private String status;
    private int count;
    private long queryDuration;
    private boolean isCollectionResult;
    private I result;
    private Collection<I> results;
    private Pagination pagination;
    private Error error;


    public static ServiceResponse errorResponse(String serviceApiVersion,
                                                String environment,
                                                Error error){


        return new ServiceResponse(serviceApiVersion,
                                   environment,
                                   error);

    }


    /** Constructs Success ServiceResponse for a SINGLE result. */
    public  ServiceResponse(String serviceApiVersion,
                            String environment,
                            I result) {

        this.serviceApiVersion = serviceApiVersion;
        this.environment = environment;
        this.status = "success";
        this.result = result;
        this.count = (result == null) ? 0 : 1;
        this.isCollectionResult = false;

    }

    /** Constructs a Success ServiceResponse for a COLLECTION result. */
    public ServiceResponse (String serviceApiVersion,
                            String environment,
                            Collection<I> results,
                            Pagination pagination) {
        this.serviceApiVersion = serviceApiVersion;
        this.environment = environment;
        this.status = "success";
        this.results = results;
        this.pagination = pagination;
        this.isCollectionResult = true;
        this.count = (results == null) ? 0 : results.size();
    }

    /** Constructs an Error ServiceResponse.*/
    private ServiceResponse(String serviceApiVersion,
                           String environment,
                           Error error) {
        this.serviceApiVersion = serviceApiVersion;
        this.environment = environment;
        this.status = "error";
        this.error = error;
    }

    public String getServiceApiVersion() {
        return serviceApiVersion;
    }

    public String getEnvironment() {
        return environment;
    }

    public int getCount() {
        return count;
    }

    public long getQueryDuration() {
        return queryDuration;
    }

    public void setQueryDuration(long duration){ this.queryDuration = duration; }

    public boolean isCollectionResult() {
        return isCollectionResult;
    }

    public I getResult() {
        return result;
    }

    public Collection<I> getResults() {
        return results;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public Error getError() {
        return error;
    }

    /**
     * This is class is used to return the details of any non-recoverable
     *  errors that occurred during the process of undertaking the request.
     *  */
    public static class Error{

        private String errorCode;
        private String errorDescription;
        private String errorDeveloperLogTraceId;

        public static Error newInst(String errorCode,
                                    String errorDescription,
                                    String errorDeveloperLogTraceId) {

            return new Error(errorCode, errorDescription,errorDeveloperLogTraceId);
        }

        public Error(String errorCode, String errorDescription, String errorDeveloperTraceId) {

            this.errorCode = errorCode;
            this.errorDescription = errorDescription;
            this.errorDeveloperLogTraceId = errorDeveloperTraceId;
        }

        public String getErrorCode() {
            return errorCode;
        }

        public String getErrorDescription() {
            return errorDescription;
        }

        public String getErrorDeveloperLogTraceId() {
            return errorDeveloperLogTraceId;
        }
    }

    /**
     * Where we have a collection of results, pagination defines the volume
     * at which these results will be delivered (i.e number of pages and results per page)
     * any applicable metadata around their delivery is also provided (e.g the total number of results available,etc)
     * */
       public static class Pagination{

        private final int resultsPerPage;
        private final int currentPage;
        private final int totalPageCount;
        private final long totalCount;
        private final String viewingWindow;

        public Pagination(int resultsPerPage,
                          int currentPage,
                          int totalPageCount,
                          long totalCount) {

            this.resultsPerPage = resultsPerPage;
            this.currentPage = currentPage;
            this.totalPageCount = totalPageCount;
            this.totalCount = totalCount;
            this.viewingWindow = computeViewingWindow(resultsPerPage,currentPage,totalCount);
        }

        public static Pagination newInst(int pageCount,
                                         int currentPage,
                                         int totalPages,
                                         long totalCount){

           return new Pagination(pageCount,
                                 currentPage,
                                 totalPages,
                                 totalCount);
        }


        public int getResultsPerPage() {
            return resultsPerPage;
        }

        public int getCurrentPage() {
            return currentPage;
        }

        public int getTotalPageCount() {
            return totalPageCount;
        }

        public long getTotalCount() {
            return totalCount;
        }

        public String getViewingWindow() {return viewingWindow;}

        private static String computeViewingWindow(int resultsPerPage, int currentPage, long totalResults){

            if(totalResults <= 0 || currentPage <= 0)
                return "Viewing 0 to 0 of 0";

            long maxPages = ((long)totalResults/resultsPerPage);
            if((totalResults%resultsPerPage) > 0) maxPages+=1;
            long finalCurrentPage = ((long)currentPage > maxPages) ? maxPages : currentPage;
            long upperBound = ((long) resultsPerPage * finalCurrentPage);
            long lowerBound = ((long) upperBound - (resultsPerPage-1));
            upperBound = Math.min(upperBound, totalResults);

            final var viewingWindowStr="Viewing ".concat(Long.toString(lowerBound))
                    .concat(" to ")
                    .concat(Long.toString(upperBound))
                    .concat(" of ")
                    .concat(Long.toString(totalResults));


            return viewingWindowStr;
        }
    }

}
