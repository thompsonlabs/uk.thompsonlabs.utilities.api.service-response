package uk.thompsonlabs.utilities.api.service_response.interceptors.auth;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/** API Passport Auth , really just used as a marker annotation to
 *  enable the APIPassportAuthInterceptor to only operate on the
 *  subset of methods that have been explicitly annotated.*/

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface APAuth {
}
