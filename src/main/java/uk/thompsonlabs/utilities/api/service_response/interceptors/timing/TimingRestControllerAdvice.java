package uk.thompsonlabs.utilities.api.service_response.interceptors.timing;

import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;
import uk.thompsonlabs.utilities.api.service_response.context.APIRequestContext;
import uk.thompsonlabs.utilities.api.service_response.ServiceResponse;

import java.util.logging.Level;
import java.util.logging.Logger;

@RestControllerAdvice
public class TimingRestControllerAdvice implements ResponseBodyAdvice<ServiceResponse> {


    @Override
    public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {

        //here we ONLY target methods with the TIME annotation.
        var timeAnnotation = returnType.getMethod().getAnnotation(Time.class);

        return timeAnnotation != null;
    }

    @Override
    public ServiceResponse beforeBodyWrite(ServiceResponse serviceResponse, MethodParameter returnType, MediaType selectedContentType, Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {

        long requestDuration = APIRequestContext.getInstance().logEndTime();

        Logger.getLogger(TimingRestControllerAdvice.class.getName()).log(Level.INFO,"Time annotation found, injecting Method: "+returnType.getMethod().getName()+" execution time: "+requestDuration+" into response.");

        serviceResponse.setQueryDuration(requestDuration);

        return  serviceResponse;
    }
}
