package uk.thompsonlabs.utilities.api.service_response.interceptors.auth;



import org.springframework.core.env.Environment;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class AuthContext {

    private final String callerId;

    private final String callerSessionId;

    private final int requiredUserLevel;

    private final int callerUserLevel;

    private final boolean verifySignature;

    private final String gatewayAuthRequestTimestamp;

    private final String gatewayAuthRequestSignature;


    //used to obtain the GATEWAY_AUTH_REQUEST_SIGNATURE_KEY environment
    //variable which would have been provided to the (PIPEDREAM-derived) service
    //this utility is being utilised in as a pipeline variable.
    private final Environment springEnvironment;

    private final boolean internalCallsEnabled;

    private final String internalCallId;

    private static final Logger logger = Logger.getLogger(AuthContext.class.getName());

    private AuthContext(final String callerId,
                        final String callerUserLevelStr,
                        final String callerSessionId,
                        final int requiredUserLevel,
                        boolean verifySignature,
                        final String gatewayAuthRequestTimestamp,
                        final String gatewayAuthRequestSignature,
                        final Environment springEnvironment,
                        final boolean internalCallsEnabled,
                        final String internalCallId) throws AuthenticationException, AuthorizationException {


        //if an internal call id has been specified....
        if(!(internalCallId == null || internalCallId.isEmpty() || internalCallId.isBlank())){

            //if internal calls are NOT enabled we cannot continue and simply log a message to that effect.
            if(!internalCallsEnabled){
                logger.log(Level.WARNING,"An internal call ID was specified, however internal calls ARE NOT currently enabled for this endpoint. Standard API Gateway Signature Key verification will apply to this request");
            }else{

                logger.log(Level.INFO,"Validating internal call.....");

                //conversely where internal calls ARE enabled, we validate the internal call id where
                //this succeeds we then explicitly set the verifySignature key to FALSE in order
                //to skip gatewayAuthRequestSignature key verification for the current request.
                final var thisServicesGatewayAuthRequestSignatureKey = springEnvironment.getProperty("gateway.auth.request.signature.key");

                //the internalCallId must be a gateway signature key that MATCHES the value associated with THIS service...
                if(thisServicesGatewayAuthRequestSignatureKey != null
                   &&thisServicesGatewayAuthRequestSignatureKey.equals(internalCallId)){

                    //we may now safely disable gateway signature verification as this is a valid internal,
                    //direct service-to-service request.
                    verifySignature = false;

                    //log status message
                    logger.log(Level.INFO,"Successfully validated internal call; API Gateway Signature key verification has been disabled for this request.");
                }else{

                    logger.log(Level.WARNING,"Validation of internal call failed. Standard API Gateway Signature Key verification will apply to this request.");
                }

            }
        }


        if(callerId == null
                || callerId.isEmpty()
                || callerId.isBlank())
            throw new AuthenticationException("Unauthorised. A null or invalid Caller ID was specified.");

        if(callerUserLevelStr == null
                || callerUserLevelStr.isEmpty()
                || callerUserLevelStr.isBlank())
            throw new AuthenticationException("Unauthorised. A null or invalid Caller User Level was specified.");

        if(callerSessionId == null
                || callerSessionId.isEmpty()
                || callerSessionId.isBlank())
            throw new AuthenticationException("Unauthorised. A null or invalid Caller Session ID was specified.");

        if(verifySignature && (gatewayAuthRequestTimestamp == null
                || gatewayAuthRequestTimestamp.isEmpty()
                || gatewayAuthRequestTimestamp.isBlank()))
            throw new AuthenticationException("Unauthorised. A null or invalid Gateway Auth Request Timestamp was specified.");


        if(verifySignature && (gatewayAuthRequestSignature == null
                || gatewayAuthRequestSignature.isEmpty()
                || gatewayAuthRequestSignature.isBlank()))
            throw new AuthenticationException("Unauthorised. A null or invalid Gateway Auth Request Signature was specified.");


        //attempt to parse supplied caller user level string to int
        try {

            this.callerUserLevel = Integer.parseInt(callerUserLevelStr);
        }
        catch(NumberFormatException nfe){

            throw new AuthenticationException("Unauthorised. A null or invalid Caller User Level was specified: "+nfe.getMessage(),nfe);
        }

        //set the remainder of our instance vars...
        this.callerId = callerId;

        this.callerSessionId = callerSessionId;

        this.verifySignature = verifySignature;

        this.gatewayAuthRequestTimestamp = gatewayAuthRequestTimestamp;

        this.gatewayAuthRequestSignature = gatewayAuthRequestSignature;

        this.requiredUserLevel = requiredUserLevel;

        this.springEnvironment = springEnvironment;

        this.internalCallsEnabled = internalCallsEnabled;

        this.internalCallId = internalCallId;

        checkAuthorization(this.callerId,this.callerSessionId,this.callerUserLevel,this.requiredUserLevel,this.verifySignature,this.gatewayAuthRequestTimestamp,this.gatewayAuthRequestSignature,this.springEnvironment);

    }

    /**
     *  Creates new AuthContext instance.
     *  NB: This factory method is ONLY intended for use by the AuthInterceptor and SHOULD NOT be
     *      accessed directly.
     * */
    static AuthContext newInst(final String callerId,
                               final String callerUserLevel,
                               final String callerSessionId,
                               final int requiredUserLevel,
                               final boolean verifySignature,
                               final String gatewayAuthRequestTimestamp,
                               final String gatewayAuthRequestSignature,
                               final Environment springEnvironment,
                               final boolean internalCallsEnabled,
                               final String internalCallId) throws AuthenticationException, AuthorizationException {

        return new AuthContext(
                callerId,callerUserLevel,
                callerSessionId,
                requiredUserLevel,
                verifySignature,
                gatewayAuthRequestTimestamp,
                gatewayAuthRequestSignature,
                springEnvironment,
                internalCallsEnabled,
                internalCallId);
    }

    /** Alternative constructor, creates AuthContext from a Map of header name-value pairs.
    public static AuthContext fromHeaders(final Map<String,String> headersNameValuePairs,
                               final int requiredUserLevel,
                               final String callerIdHeaderName,
                               final String callerUserLevelHeaderName,
                               final String callerSessionIdHeaderName) throws AuthenticationException, AuthorizationException {


        var callerId = headersNameValuePairs.get(callerIdHeaderName);
        var callerUserLevel = headersNameValuePairs.get(callerUserLevelHeaderName);
        var callerSessionId = headersNameValuePairs.get(callerSessionIdHeaderName);

        return new AuthContext(callerId,callerUserLevel,callerSessionId,requiredUserLevel);
    }

     */
    public String getCallerId() {
        return callerId;
    }

    public int getRequiredUserLevel() {
        return requiredUserLevel;
    }

    public int getCallerUserLevel() {
        return callerUserLevel;
    }

    public String getCallerSessionId() {
        return callerSessionId;
    }

    public boolean isVerifySignature() {
        return verifySignature;
    }

    public String getGatewayAuthRequestTimestamp() {
        return gatewayAuthRequestTimestamp;
    }

    public String getGatewayAuthRequestSignature() {
        return gatewayAuthRequestSignature;
    }

    private static void checkAuthorization(String callerId,
                                           String callerSessionId,
                                           int specifiedUserLevel,
                                           int requiredUserLevel,
                                           boolean verifyGatewayAuthRequestSignature,
                                           String gatewayAuthRequestTimestamp,
                                           String gatewayAuthRequestSignature,
                                           Environment springEnvironment) throws AuthorizationException {

        //first and foremost we verify the provided signature if we are required to.
        if(verifyGatewayAuthRequestSignature)
              verifyGatewayAuthReqSignature(callerId,callerSessionId,specifiedUserLevel,gatewayAuthRequestTimestamp,gatewayAuthRequestSignature,springEnvironment);


        //finally verify that user has sufficient privileges.
        if(specifiedUserLevel < requiredUserLevel)
            throw new AuthorizationException("Forbidden. The caller does not have sufficient privileges to view the specified resource.");
    }

    private static void verifyGatewayAuthReqSignature(String callerId,
                                                      String callerSessionId,
                                                      int specifiedUserLevel,
                                                      String gatewayAuthRequestTimestamp,
                                                      String gatewayAuthRequestSignature,
                                                      Environment springEnvironment) throws AuthorizationException {


        //Grab  reference to the API Gateway Service's private signature key (i.e the key it uses to sign request
        //from the current environment. (All PIPEDREAM-derived) services will have this environment variable set.
        final var gatewayServiceAuthRequestSignatureKey = springEnvironment.getProperty("gateway.auth.request.signature.key");

        if(gatewayServiceAuthRequestSignatureKey == null
           || gatewayServiceAuthRequestSignatureKey.isBlank()
           || gatewayServiceAuthRequestSignatureKey.equals("\"\""))
            throw new AuthorizationException("Forbidden. Unable to authorise request; an error occurred whilst attempting to resolve requisite gw auth signature key value.");

        //Compose plain-text signature key from our supplied parameters...
        final var ptSig = callerSessionId
                .concat(gatewayAuthRequestTimestamp)
                .concat(Integer.toString(specifiedUserLevel))
                .concat(callerId)
                .concat(gatewayServiceAuthRequestSignatureKey);

        //generate sha512 hash of the plain text signature.
        final var hSig = generateSha512Hash(ptSig);

        //finally compare locally generated hashed signature with the value provided to us
        //where they do not match throw an appropriate exception...
        if(!hSig.equals(gatewayAuthRequestSignature))
            throw new AuthorizationException("Forbidden. Gateway Auth Request Signature Mismatch.");

    }

    private static String generateSha512Hash(String aPlainTextStr) {

        try {
            // getInstance() method is called with algorithm SHA-512
            MessageDigest md = MessageDigest.getInstance("SHA-512");

            // digest() method is called
            // to calculate message digest of the input string
            // returned as array of byte
            byte[] messageDigest = md.digest(aPlainTextStr.getBytes());

            // Convert byte array into signum representation
            BigInteger no = new BigInteger(1, messageDigest);

            // Convert message digest into hex value
            String hashtext = no.toString(16);

            // Add preceding 0s to make it 32 bit
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }

            // return the HashText
            return hashtext;
        }

        // For specifying wrong message digest algorithms
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }


    //HELPER CLASSES

    public static class AuthenticationException extends Exception{

        public AuthenticationException() {
        }

        public AuthenticationException(String message) {
            super(message);
        }

        public AuthenticationException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    public static class AuthorizationException extends Exception{

        public AuthorizationException() {
        }

        public AuthorizationException(String message) {
            super(message);
        }

        public AuthorizationException(String message, Throwable cause) {
            super(message, cause);
        }
    }
}
